# Changes in srhinow/themecontent-bundle

## 0.2.2 (19.06.2023)
- fix alias field and 'deleteConfirm'

## 0.2.1 (20.10.2022)
- update README.md
- ModuleThemeArticle in src/Module verschoben und Namespace von Modules in Module angepasst
- add ModuleThemeArticle
- fix sql definitions from alias dca-fields

## 0.2.0 (25.02.2020)
- namespace Srhinow -> Srhinow\ThemecontentBundle\Modules, clean code
- fix act - permissions
- drop toggle, intertag in theme article list
- add Frontend-Modul Theme Artikel
- findById -> findByPk
- Artikel-Modul so geändert das es sowohl als Frontend-Modul als auch per InsertTag aufgerufen wird
- run code fixer

## ...

## 0.1.0 (20.12.2017)
- first commit