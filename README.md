# Theme Inhalte (Contao4-Modul)

Hiermit werden im Contao-Backend für z.B. Footer oder Header-Inhaltselemente, nicht mehr wie sonst üblich Fake-Seitenbäume angelegt, sondern eine eigene Struktur geschaffen. 

## Motivation: 
Die in Contao gern genutzte saubere Trennung von bestimmten Aufgaben habe ich hiermit versucht, auch auf die Trennung von Themespezifischen Inhalten und Seitenspezifischen Inahlten auszubauen.
Inhaltselemente werden weiterhin auch in der tl_content abgelegt. So kann wie gewohnt z.B. mit Inserttags {{insert_content::ID|ALIAS}} darauf zugegriffen werden und bei einem Import/Export kann gezielt nach ptable = 'tl_theme_section_article'  gefiltert werden.

### Insert-Tags
* Für Theme-Artikel wird der {{insert_theme_article::ID|ALIAS}} verwendet.
* Für einzelne Content-Elemente aus dem Theme-Inhalte-Bereich kann sowohl {{insert_content::ID|ALIAS}} als auch {{insert_theme_content::ID|ALIAS}} verwendet werden.

### Content-Element
Es gibt die Möglichkeit mit dem Content-Element "Theme-Artikel" und einem Selectfeld ein bestimmten Artikel aus den Theme Inhalten einzubinden.

### Frontend-Modul
Es gibt die Möglichkeit ein Frontend-Modul "Theme Inhalte" / "Theme Artikel" per Selectfeld ein bestimmten Artikel aus den Theme Inhalten anzulegen und dann wie gewohnt entweder per Seitenlayout oder als Inhaltelement einzubinden.

### Hooks
Als Hook zum Überschreiben der Theme-Artikel-Ausgabe, kann $GLOBALS['TL_HOOKS']['compileThemeArticle'] wie bei $GLOBALS['TL_HOOKS']['compileArticle'] verwendet werden.

Diese werden durch 

* Theme-Inhalte
  * Bereiche ("tl_theme_section" wie Pages), 
  * Bereich-Artikel ("tl_theme_section_article" Baumstruktur nach Bereiche wie "Artikel" mit ptable "tl_theme_section")
    * Inhaltselemente ("tl_content" mit ptable "tl_theme_section_article")
    
verwaltet.


