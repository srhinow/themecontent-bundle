<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['themecontent_article'] = '
    {title_legend},name,headline,type;
    {config_legend},themeArticle;
    {protected_legend:hide},protected;
    {expert_legend:hide},guests,cssID,space
    ';

/*
 * Fields
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['themeArticle'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_module']['themeArticle'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow_themecontent.listener.dca.content', 'onOptionsThemeArticle'],
    'eval' => ['mandatory' => true, 'chosen' => true, 'submitOnChange' => true, 'tl_class' => 'w50 wizard'],
    'wizard' => [['srhinow_themecontent.listener.dca.content', 'onEditThemeArticle']],
    'sql' => "int(10) unsigned NOT NULL default '0'",
];
