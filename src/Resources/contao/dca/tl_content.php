<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Dynamically add the parent table.
 */
if ('theme_content' === Input::get('do')) {
    $GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_theme_section_article';
}

/*
 * Fields
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['themeArticle'] =
[
    'label' => &$GLOBALS['TL_LANG']['tl_content']['themeArticle'],
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => ['srhinow_themecontent.listener.dca.content', 'onOptionsThemeArticle'],
    'eval' => ['mandatory' => true, 'chosen' => true, 'submitOnChange' => true, 'tl_class' => 'w50 wizard'],
    'wizard' => [['srhinow_themecontent.listener.dca.content', 'onEditThemeArticle']],
    'sql' => "int(10) unsigned NOT NULL default '0'",
];

/*
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_content']['palettes']['themeArticle'] = str_replace(
    'articleAlias',
    'themeArticle',
    $GLOBALS['TL_DCA']['tl_content']['palettes']['article']
);
