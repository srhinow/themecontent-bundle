<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Load class tl_page.
 */
$this->loadDataContainer('tl_page');

/*
 * Table tl_theme_section
 */
$GLOBALS['TL_DCA']['tl_theme_section'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => ['tl_theme_section_article'],
        'switchToEdit' => true,
        'enableVersioning' => true,
        'onload_callback' => [
            ['srhinow_themecontent.listener.dca.theme_section', 'checkPermission'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'index',
                'pid,published' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 5,
            'icon' => 'pagemounts.svg',
            'panelLayout' => 'filter;search',
        ],
        'label' => [
            'fields' => ['title'],
            'format' => '%s',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            ],
        ],
        'operations' => [
            'theme_section_article' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['theme_section_article'],
                'href' => 'table=tl_theme_section_article',
                'icon' => 'bundles/srhinowthemecontent/icons/combo_boxes.png',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section', 'editArticles'],
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['editheader'],
                'href' => 'act=edit',
                'icon' => 'header.svg',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'cut' => [
                'label' => &$GLOBALS['TL_LANG']['tl_page']['cut'],
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                return false; Backend.getScrollOffset();"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['protected'],
        'default' => '{title_legend},title,alias,description,published',
    ],
    // Subpalettes
    'subpalettes' => [
        'protected' => 'groups',
    ],
    // Fields
    'fields' => [
        'id' => [
            'label' => ['ID'],
            'search' => true,
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['title'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['alias'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => array('rgxp'=>'alias', 'doNotCopy'=>true, 'unique'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
            'save_callback' => [
                ['srhinow_themecontent.listener.dca.theme_section', 'generateAlias'],
            ],
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ],
        'type' => [
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'pageTitle' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'language' => [
            'sql' => "varchar(5) NOT NULL default ''",
        ],
        'robots' => [
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'description' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['description'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['style' => 'height:60px', 'decodeEntities' => true, 'tl_class' => 'clr'],
            'sql' => 'text NULL',
        ],
        'redirect' => [
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'jumpTo' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'redirectBack' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'url' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'target' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'dns' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'staticFiles' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'staticPlugins' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'fallback' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'adminEmail' => [
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'dateFormat' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['dateFormat'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['helpwizard' => true, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'explanation' => 'dateFormat',
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'timeFormat' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['timeFormat'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'datimFormat' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['datimFormat'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'createSitemap' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'sitemapName' => [
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'useSSL' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'autoforward' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'protected' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['protected'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'groups' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['groups'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'foreignKey' => 'tl_member_group.name',
            'eval' => ['mandatory' => true, 'multiple' => true],
            'sql' => 'blob NULL',
            'relation' => ['type' => 'hasMany', 'load' => 'lazy'],
        ],
        'includeLayout' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'layout' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'mobileLayout' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'includeCache' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['includeCache'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'cache' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['cache'],
            'default' => 0,
            'exclude' => true,
            'inputType' => 'select',
            'options' => [0, 5, 15, 30, 60, 300, 900, 1800, 3600, 10800, 21600, 43200, 86400, 259200, 604800, 2592000],
            'reference' => &$GLOBALS['TL_LANG']['CACHE'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'clientCache' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['clientCache'],
            'default' => 0,
            'exclude' => true,
            'inputType' => 'select',
            'options' => [0, 5, 15, 30, 60, 300, 900, 1800, 3600, 10800, 21600, 43200, 86400, 259200, 604800, 2592000],
            'reference' => &$GLOBALS['TL_LANG']['CACHE'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'includeChmod' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['includeChmod'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'cuser' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['cuser'],
            'default' => (int) (Config::get('defaultUser')),
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_user.name',
            'eval' => ['mandatory' => true, 'chosen' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'cgroup' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['cgroup'],
            'default' => (int) (Config::get('defaultGroup')),
            'exclude' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_user_group.name',
            'eval' => ['mandatory' => true, 'chosen' => true, 'includeBlankOption' => true, 'tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
        ],
        'chmod' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['chmod'],
            'default' => Config::get('defaultChmod'),
            'exclude' => true,
            'inputType' => 'chmod',
            'eval' => ['tl_class' => 'clr'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'noSearch' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['noSearch'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'sql' => "char(1) NOT NULL default ''",
        ],
        'cssClass' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['cssClass'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 64, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default ''",
        ],
        'sitemap' => [
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'hide' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['hide'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'guests' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['guests'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'tabindex' => [
            'sql' => "smallint(5) unsigned NOT NULL default '0'",
        ],
        'accesskey' => [
            'sql' => "char(1) NOT NULL default ''",
        ],
        'published' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['published'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'start' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['start'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'stop' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section']['stop'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
    ],
];
