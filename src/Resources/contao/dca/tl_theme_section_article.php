<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * Load class tl_theme_section.
 */
$this->loadDataContainer('tl_theme_section');

/*
 * Table tl_theme_section_article
 */
$GLOBALS['TL_DCA']['tl_theme_section_article'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_theme_section',
        'ctable' => ['tl_content'],
        'switchToEdit' => true,
        'enableVersioning' => true,
        'onload_callback' => [
            ['srhinow_themecontent.listener.dca.theme_section_article', 'checkPermission'],
            ['srhinow_themecontent.listener.dca.theme_section', 'addBreadcrumb'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'alias' => 'index',
                'pid,start,stop,published,sorting' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 6,
            'fields' => ['published DESC', 'title', 'author'],
            'paste_button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'pasteArticle'],
            'panelLayout' => 'filter;search',
        ],
        'label' => [
            'fields' => ['title', 'id'],
            'format' => '%s <span style="color:#999;padding-left:3px">{{insert_theme_article::%s}}</span>',
            'label_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'addIcon'],
        ],
        'global_operations' => [
            'theme_section' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['back_to_sectione_overview'],
                'href' => 'do=theme_content&amp;table=',
                'icon' => 'back.svg',
                //                'button_callback'     => array('tl_theme', 'editCss')
            ],
            'toggleNodes' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['toggleAll'],
                'href' => '&amp;ptg=all',
                'class' => 'header_toggle',
                'showOnSelect' => true,
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['edit'],
                'href' => 'table=tl_content',
                'icon' => 'edit.svg',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'editArticle'],
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['editheader'],
                'href' => 'act=edit',
                'icon' => 'header.svg',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'editHeader'],
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['copy'],
                'href' => 'act=paste&amp;mode=copy',
                'icon' => 'copy.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'copyArticle'],
            ],
            'cut' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['cut'],
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.svg',
                'attributes' => 'onclick="Backend.getScrollOffset()"',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'cutArticle'],
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                return false; Backend.getScrollOffset();"',
                'button_callback' => ['srhinow_themecontent.listener.dca.theme_section_article', 'deleteArticle'],
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
            ],
        ],
    ],

    // Select
    'select' => [
        'buttons_callback' => [
            ['srhinow_themecontent.listener.dca.theme_section_article', 'addAliasButton'],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['protected'],
        'default' => '
            {title_legend},title,alias;
            {protected_legend:hide},protected;
            {expert_legend:hide},guests,cssID;
            {publish_legend},published',
    ],

    // Subpalettes
    'subpalettes' => [
        'protected' => 'groups',
    ],

    // Fields
    'fields' => [
        'id' => [
            'label' => ['ID'],
            'search' => true,
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_page.title',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['title'],
            'exclude' => true,
            'inputType' => 'text',
            'search' => true,
            'eval' => ['mandatory' => true, 'decodeEntities' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'alias' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['alias'],
            'exclude' => true,
            'inputType' => 'text',
            'search' => true,
            'eval' => ['rgxp'=>'alias', 'doNotCopy'=>true, 'unique'=>true, 'maxlength'=>255, 'tl_class' => 'w50 clr'],
            'save_callback' => [
                ['srhinow_themecontent.listener.dca.theme_section_article', 'generateAlias'],
            ],
            'sql' => "varchar(255) BINARY NOT NULL default ''",
        ],
        'author' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['author'],
            'default' => BackendUser::getInstance()->id,
            'exclude' => true,
            'search' => true,
            'filter' => true,
            'inputType' => 'select',
            'foreignKey' => 'tl_user.name',
            'eval' => [
                'doNotCopy' => true,
                'mandatory' => true,
                'chosen' => true,
                'includeBlankOption' => true,
                'tl_class' => 'w50',
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'hasOne', 'load' => 'eager'],
        ],
        'inColumn' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['inColumn'],
            'exclude' => true,
            'filter' => true,
            'default' => 'main',
            'inputType' => 'select',
            'options_callback' => [
                'srhinow_themecontent.listener.dca.theme_section_article',
                'getActiveLayoutSections',
            ],
            'eval' => ['tl_class' => 'w50'],
            'reference' => &$GLOBALS['TL_LANG']['COLS'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'keywords' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['keywords'],
            'exclude' => true,
            'inputType' => 'textarea',
            'search' => true,
            'eval' => ['style' => 'height:60px', 'decodeEntities' => true, 'tl_class' => 'clr'],
            'sql' => 'text NULL',
        ],
        'showTeaser' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['showTeaser'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50 m12'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'teaserCssID' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['teaserCssID'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['multiple' => true, 'size' => 2, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'teaser' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['teaser'],
            'exclude' => true,
            'inputType' => 'textarea',
            'search' => true,
            'eval' => ['rte' => 'tinyMCE', 'tl_class' => 'clr'],
            'sql' => 'text NULL',
        ],
        'printable' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['printable'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'options' => ['print', 'pdf', 'facebook', 'twitter', 'gplus'],
            'eval' => ['multiple' => true],
            'reference' => &$GLOBALS['TL_LANG']['tl_theme_section_article'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'customTpl' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['customTpl'],
            'exclude' => true,
            'inputType' => 'select',
            'options_callback' => [
                'srhinow_themecontent.listener.dca.theme_section_article',
                'getArticleTemplates',
            ],
            'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default ''",
        ],
        'protected' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['protected'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'groups' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['groups'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'foreignKey' => 'tl_member_group.name',
            'eval' => ['mandatory' => true, 'multiple' => true],
            'sql' => 'blob NULL',
            'relation' => ['type' => 'hasMany', 'load' => 'lazy'],
        ],
        'guests' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['guests'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'cssID' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['cssID'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['multiple' => true, 'size' => 2, 'tl_class' => 'w50 clr'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'published' => [
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['published'],
            'exclude' => true,
            'filter' => true,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'start' => [
            'exclude' => true,
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['start'],
            'inputType' => 'text',
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'stop' => [
            'exclude' => true,
            'label' => &$GLOBALS['TL_LANG']['tl_theme_section_article']['stop'],
            'inputType' => 'text',
            'eval' => ['rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
    ],
];

/* CTS-Fields for CTS-Contao-theme-Modules */
if (class_exists('\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper')) {
    $strName = 'tl_theme_section_article';

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsarticlebg'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsarticlebg'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsbordertop'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsbordertop'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsborderbottom'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsborderbottom'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridoffsetxs'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridoffsetxs'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridoffsetsm'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridoffsetsm'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridoffsetmd'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridoffsetmd'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridoffsetlg'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridoffsetlg'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridclassxs'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridclassxs'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridclasssm'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridclasssm'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridclassmd'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridclassmd'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsgridclasslg'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsgridclasslg'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsmargintop'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsmargintop'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctsmarginbottom'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctsmarginbottom'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctspaddingtop'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctspaddingtop'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];

    $GLOBALS['TL_DCA'][$strName]['fields']['ctspaddingbottom'] = [
        'label' => &$GLOBALS['TL_LANG'][$strName]['ctspaddingbottom'],
        'default' => '',
        'exclude' => true,
        'inputType' => 'select',
        'eval' => ['includeBlankOption' => true, 'tl_class' => 'w50'],
        'options_callback' => ['\\esit\\ctscore\\classes\\contao\\helper\\OptionHelper', 'getOptionsByField'],
        'sql' => "varchar(255) NOT NULL default ''",
    ];
}
