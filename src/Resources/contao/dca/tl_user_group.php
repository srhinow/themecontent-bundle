<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

Contao\CoreBundle\DataContainer\PaletteManipulator::create()
    ->addLegend(
        'themecontent_legend',
        'tl_user_group',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_BEFORE
    )
    ->addField(
        [
            'themecontent_sectionp',
            'themecontent_section_articles',
            'themecontent_section_articlep',
        ],
        'themecontent_legend',
        Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND
    )
    ->applyToPalette('default', 'tl_user_group')
;

// Add fields to tl_user
$GLOBALS['TL_DCA']['tl_user_group']['fields']['themecontent_sectionp'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user_group']['themecontent_sectionp'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'create', 'copy', 'cut', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user_group']['fields']['themecontent_section_articles'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user_group']['themecontent_section_articles'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_theme_section_article.title',
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user_group']['fields']['themecontent_section_articlep'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user_group']['themecontent_section_articlep'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['edit', 'editHeader', 'create', 'copy', 'cut', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];
