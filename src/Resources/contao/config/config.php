<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------.
 */
array_insert($GLOBALS['BE_MOD']['design'], 1, [
    'theme_content' => [
        'tables' => ['tl_theme_section', 'tl_theme_section_article', 'tl_content'],
        'table' => ['TableWizard', 'importTable'],
        'list' => ['ListWizard', 'importList'],
    ],
]);

/*
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'themecontent_sections';
$GLOBALS['TL_PERMISSIONS'][] = 'themecontent_sectionp';
$GLOBALS['TL_PERMISSIONS'][] = 'themecontent_section_articles';
$GLOBALS['TL_PERMISSIONS'][] = 'themecontent_section_articlep';

/*
 * Models
 */
$GLOBALS['TL_MODELS']['tl_theme_section'] = \Srhinow\ThemecontentBundle\Model\ThemeSectionModel::class;
$GLOBALS['TL_MODELS']['tl_theme_section_article'] = \Srhinow\ThemecontentBundle\Model\ThemeSectionArticleModel::class;

/*
 * Hooks
 */
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = ['srhinow_themecontent.listener.hook.insert_tags', 'onReplaceInsertTags'];

/*
 * Content elements
 */
$GLOBALS['TL_CTE']['includes']['themeArticle'] = \Srhinow\ThemecontentBundle\ContentElement\ContentThemeArticle::class;

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['FE_MOD'], 2, [
    'themecontent' => [
        'themecontent_article' => 'Srhinow\ThemecontentBundle\Module\ModuleThemeArticle',
    ],
]);
