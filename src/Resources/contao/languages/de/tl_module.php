<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['tl_module']['themeArticle'] = [
    'Theme Inhalte :: Artikel',
    'Bitte wählen Sie den Theme Inhalte :: Artikel aus, den Sie einfügen möchten.',
];
