<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

\System::loadLanguageFile('tl_article');
$GLOBALS['TL_LANG']['tl_theme_section_article'] = $GLOBALS['TL_LANG']['tl_article'];
$GLOBALS['TL_LANG']['tl_theme_section_article']['back_to_sectione_overview'] = 'zur Bereichsübersicht';
