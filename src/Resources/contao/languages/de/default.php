<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

$GLOBALS['TL_LANG']['CTE']['themeArticle'] = ['Theme Artikel', 'Inkludiert einen Theme Artikel.'];

/*
 * Front end modules.
 */
$GLOBALS['TL_LANG']['FMD']['themecontent'] = ['Theme Inhalte', 'von der Seitenstruktur unabhängige Inhalte'];
$GLOBALS['TL_LANG']['FMD']['themecontent_article'] = ['Theme Artikel', 'Die Inhalte dieses Theme Artikels einfügen.'];
