<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\Module;

use Contao\ContentModel;
use Contao\Module;

/**
 * Provides methodes to handle articles.
 *
 * @property int    $tstamp
 * @property string $title
 * @property string $alias
 * @property string $inColumn
 * @property bool   $showTeaser
 * @property bool   $published
 * @property int    $start
 * @property int    $stop
 */
class ModuleThemeArticle extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_article';

    /**
     * No markup.
     *
     * @var bool
     */
    protected $blnNoMarkup = false;

    /**
     * Check whether the article is published.
     *
     * @param bool $blnNoMarkup
     *
     * @return string
     */
    public function generate($blnNoMarkup = false)
    {
        if (TL_MODE === 'BE') {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### THEME ARTICLE ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        $this->type = 'article';
        $this->blnNoMarkup = $blnNoMarkup;

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        $id = 'article-'.$this->id;

        // Generate the CSS ID if it is not set
        if (empty($this->cssID[0])) {
            $this->cssID = [$id, $this->cssID[1]];
        }

        $this->Template->column = $this->inColumn;
        $this->Template->noMarkup = $this->blnNoMarkup;

        // Add the modification date
        $this->Template->timestamp = $this->tstamp;
        $this->Template->printable = false;
        $this->Template->backlink = false;

        $arrElements = [];

        //unterscheidet ob es ein Frontend-Modul ist oder als InsertTag aufgerufen wurde
        $articleId = (null === $this->themeArticle) ? $this->id : $this->themeArticle;

        $objCte = \ContentModel::findPublishedByPidAndTable($articleId, 'tl_theme_section_article');

        if (null !== $objCte) {
            $intCount = 0;
            $intLast = $objCte->count() - 1;

            while ($objCte->next()) {
                $arrCss = [];

                /** @var ContentModel $objRow */
                $objRow = $objCte->current();

                // Add the "first" and "last" classes (see #2583)
                if (0 === $intCount || $intCount === $intLast) {
                    if (0 === $intCount) {
                        $arrCss[] = 'first';
                    }

                    if ($intCount === $intLast) {
                        $arrCss[] = 'last';
                    }
                }

                $objRow->classes = $arrCss;
                $arrElements[] = $this->getContentElement($objRow, $this->strColumn);
                ++$intCount;
            }
        }

        $this->Template->teaser = $this->teaser;
        $this->Template->elements = $arrElements;

        if ('' !== $this->keywords) {
            $GLOBALS['TL_KEYWORDS'] .= (('' !== $GLOBALS['TL_KEYWORDS']) ? ', ' : '').$this->keywords;
        }

        // HOOK: add custom logic
        if (isset($GLOBALS['TL_HOOKS']['compileThemeArticle'])
            && \is_array($GLOBALS['TL_HOOKS']['compileThemeArticle'])) {
            foreach ($GLOBALS['TL_HOOKS']['compileThemeArticle'] as $callback) {
                $this->import($callback[0]);
                $this->{$callback[0]}->{$callback[1]}($this->Template, $this->arrData, $this);
            }
        }
    }
}
