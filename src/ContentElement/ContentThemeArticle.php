<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\ContentElement;

use Contao\ContentElement;
use Contao\ContentModel;
use Srhinow\ThemecontentBundle\Model\ThemeSectionArticleModel;

/**
 * Front end content element "theme article alias".
 */
class ContentThemeArticle extends ContentElement
{
    /**
     * Parse the template.
     *
     * @return string
     */
    public function generate()
    {
        $objRow = ThemeSectionArticleModel::findByPk($this->themeArticle);

        if (null === $objRow || !static::isVisibleElement($objRow)) {
            return '';
        }

        $objCte = ContentModel::findPublishedByPidAndTable($this->themeArticle, 'tl_theme_section_article');

        if (null === $objCte) {
            return '';
        }

        $strBuffer = '';

        while ($objCte->next()) {
            $strBuffer .= $this->getContentElement($objCte->current());
        }

        return $strBuffer;
    }

    /**
     * Generate the content element.
     */
    protected function compile(): void
    {
    }
}
