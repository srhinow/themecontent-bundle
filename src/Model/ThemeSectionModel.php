<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\Model;

use Contao\Model;

class ThemeSectionModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_theme_section';
}
