<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\Model;

use Contao\Model;

class ThemeSectionArticleModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_theme_section_article';

    /**
     * Find published article items by their ID or alias.
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     *
     * @return Model|null The ThemeSectionArticleModel or null if there are no category
     */
    public static function findThemeArticleByIdOrAlias($varId, array $arrOptions = [])
    {
        return static::findByIdOrAlias($varId, $arrOptions);
    }

    /**
     * Find categories for pagination-list.
     *
     * @param int $intLimit
     * @param int $intOffset
     *
     * @return Model\Collection|static|null
     */
    public static function findThemeArticle($intLimit = 0, $intOffset = 0, array $filter = [], array $arrOptions = [])
    {
        $t = static::$strTable;
        $arrColumns = (\count($filter) > 0) ? $filter : null;

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.id DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all category items.
     *
     * @return int
     */
    public static function countEntries(array $filter = [], array $arrOptions = [])
    {
        $arrColumns = (\count($filter) > 0) ? $filter : null;

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
