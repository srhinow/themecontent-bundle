<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Contao\System;

class ThemeSectionListener extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Check permissions to edit table tl_spree_case.
     *
     * @throws \Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission(): void
    {
        $bundles = System::getContainer()->getParameter('kernel.bundles');

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->themecontent_sections) || !\is_array($this->User->themecontent_sections)) {
            $root = [0];
        } else {
            $root = $this->User->themecontent_sections;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'themecontent_sections')) {
            $GLOBALS['TL_DCA']['tl_theme_section']['config']['closed'] = true;
        }

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('edit', 'themecontent_sectionp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                        theme-content section ID '.Input::get('id').'.');
                }
                break;
            case 'create':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('create', 'themecontent_sectionp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                        theme-content section ID '.Input::get('id').'.');
                }
                break;
            case 'copy':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('copy', 'themecontent_sectionp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                        theme-content section ID '.Input::get('id').'.');
                }
                // no break
            case 'delete':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('delete', 'themecontent_sectionp')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                        theme-content section ID '.Input::get('id').'.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act')
                    && !$this->User->hasAccess('delete', 'themecontent_sectionp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (\strlen(Input::get('act')) > 0) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' theme_section.');
                }
                break;
        }
    }

    /**
     * Return the edit header button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->canEditFieldsOf('tl_theme_section')
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Generate an "edit articles" button and return it as string.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     *
     * @return string
     */
    public function editArticles($row, $href, $label, $title, $icon)
    {
        if (!$this->User->hasAccess('article', 'modules')) {
            return '';
        }

        return  '<a href="'.$this->addToUrl($href.'&amp;pn='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'">'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Return the copy archive button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function copyArchive($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('create', 'themecontentp')
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Return the delete archive button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function deleteArchive($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->User->hasAccess('delete', 'themecontentp')
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Auto-generate an alias if it has not been set yet.
     *
     * @param mixed
     * @param DataContainer
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ('' === $varValue) {
            $autoAlias = true;
            $varValue = standardize(\StringUtil::restoreBasicEntities($dc->activeRecord->title));
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_theme_section WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * Add the breadcrumb menu.
     */
    public function addBreadcrumb(): void
    {
        self::addSectionBreadcrumb();
    }

    /**
     * Add a breadcrumb menu to the page tree.
     *
     * @param string $strKey
     *
     * @throws \RuntimeException
     */
    public static function addSectionBreadcrumb($strKey = 'tl_theme_section_note'): void
    {
        $objSession = \System::getContainer()->get('session')->getBag('contao_backend');

        // Set a new node
        if (isset($_GET['pn'])) {
            // Check the path (thanks to Arnaud Buchoux)
            if (\Validator::isInsecurePath(\Input::get('pn', true))) {
                throw new \RuntimeException('Insecure path '.\Input::get('pn', true));
            }

            $objSession->set($strKey, \Input::get('pn', true));
            Controller::redirect(preg_replace('/&pn=[^&]*/', '', \Environment::get('request')));
        }

        $intNode = $objSession->get($strKey);

        if ($intNode < 1) {
            return;
        }

        $arrIds = [];
        $arrLinks = [];

        // Generate breadcrumb trail
        if ($intNode) {
            $intId = $intNode;
            $objDatabase = \Database::getInstance();

            do {
                $objSection = $objDatabase->prepare('SELECT * FROM tl_theme_section WHERE id=?')
                    ->limit(1)
                    ->execute($intId)
                ;

                if ($objSection->numRows < 1) {
                    // Currently selected page does not exist
                    if ($intId === $intNode) {
                        $objSession->set($strKey, 0);

                        return;
                    }

                    break;
                }

                $arrIds[] = $intId;

                // No link for the active page
                if ($objSection->id === $intNode) {
                    $arrLinks[] = \Backend::addPageIcon(
                        $objSection->row(),
                        '',
                        null,
                        '',
                        true
                    ).' '.$objSection->title;
                } else {
                    $arrLinks[] = \Backend::addPageIcon(
                        $objSection->row(),
                        '',
                        null,
                        '',
                        true
                    ).' <a href="'.\Backend::addToUrl('pn='.$objSection->id).'" 
                        title="'.\StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['selectNode']).'">
                        '.$objSection->title.'</a>';
                }

                $intId = $objSection->pid;
            } while ($intId > 0);
        }

        // Limit tree
        $GLOBALS['TL_DCA']['tl_theme_section']['list']['sorting']['root'] = [$intNode];
//        print_r($GLOBALS['TL_DCA']['tl_theme_section']['list']['sorting']['root']);

        // Add root link
        $arrLinks[] = \Image::getHtml('pagemounts.svg').' <a href="'.\Backend::addToUrl('pn=0').'" 
        title="'.\StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['selectAllNodes']).'">
        '.$GLOBALS['TL_LANG']['MSC']['filterAll'].'</a>';

        $arrLinks = array_reverse($arrLinks);

        // Insert breadcrumb menu
        $GLOBALS['TL_DCA']['tl_theme_section']['list']['sorting']['breadcrumb'] .= '
        <ul id="tl_breadcrumb">
            <li>'.implode(' › </li><li>', $arrLinks).'</li>
        </ul>';
    }
}
