<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\EventListener\Dca;

use Contao\ArticleModel;
use Contao\Backend;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\LayoutModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;

class ThemeSectionArticleListener extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Check permissions to edit table tl_spree_case.
     *
     * @throws \Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission(): void
    {
        $bundles = System::getContainer()->getParameter('kernel.bundles');

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->themecontent_section_articles)
            || !\is_array($this->User->themecontent_section_articles)) {
            $root = [0];
        } else {
            $root = $this->User->themecontent_section_articles;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'themecontent_section_articlep')) {
            $GLOBALS['TL_DCA']['tl_theme_section_article']['config']['closed'] = true;
        }

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('edit', 'themecontent_section_articlep')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                    theme-content section-article ID '.Input::get('id').'.');
                }
                break;
            case 'create':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('create', 'themecontent_section_articlep')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                    theme-content section-article ID '.Input::get('id').'.');
                }
                break;
            case 'copy':
            case 'copyAll':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('copy', 'themecontent_section_articlep')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                    theme-content section-article ID '.Input::get('id').'.');
                }
                break;
            case 'cut':
            case 'cutAll':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('cut', 'themecontent_section_articlep')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                    theme-content section-article ID '.Input::get('id').'.');
                }
                break;
            case 'delete':
                // Check permissions to add spree_cm_cases
                if (!$this->User->hasAccess('delete', 'themecontent_section_articlep')) {
                    throw new AccessDeniedException('Not enough permissions to '.Input::get('act').' 
                    theme-content section-article ID '.Input::get('id').'.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$this->User->hasAccess(
                    'delete',
                    'themecontent_section_articlep'
                )) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array) $session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;
        }
    }

    /**
     * Add an image to each page in the tree.
     *
     * @param array  $row
     * @param string $label
     *
     * @return string
     */
    public function addIcon($row, $label)
    {
        $image = 'articles';
        $time = \Date::floorToMinute();

        $unpublished = '' !== $row['start'] && $row['start'] > $time || '' !== $row['stop'] && $row['stop'] < $time;

        if (!$row['published'] || $unpublished) {
            $image .= '_';
        }

        return '<a href="contao/main.php?do=feRedirect&amp;page='.$row['pid'].'&amp;article='.($row['alias']
                ?: $row['id']).'" title="'.StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['view']).'" 
                target="_blank">'.Image::getHtml(
                    $image.'.svg',
                    '',
                    'data-icon="'.($unpublished ? $image : rtrim($image, '_')).'.svg" 
                    data-icon-disabled="'.rtrim($image, '_').'_.svg"'
                ).'</a> '.$label;
    }

    /**
     * Auto-generate an article alias if it has not been set yet.
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ('' === $varValue) {
            $autoAlias = true;
            $varValue = StringUtil::generateAlias($dc->activeRecord->title);
        }

        // Add a prefix to reserved names (see #6066)
        if (\in_array(
            $varValue,
            ['top', 'wrapper', 'header', 'container', 'main', 'left', 'right', 'footer'],
            true
        )) {
            $varValue = 'article-'.$varValue;
        }

        $objAlias = $this->Database->prepare('SELECT id FROM tl_theme_section_article WHERE id=? OR alias=?')
            ->execute($dc->id, $varValue)
        ;

        // Check whether the page alias exists
        if ($objAlias->numRows > 1) {
            if (!$autoAlias) {
                throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-'.$dc->id;
        }

        return $varValue;
    }

    /**
     * Return all active layout sections as array.
     *
     * @return array
     */
    public function getActiveLayoutSections(DataContainer $dc)
    {
        // Show only active sections
        if ($dc->activeRecord->pid) {
            $arrSections = [];
            $objPage = PageModel::findWithDetails($dc->activeRecord->pid);

            // Get the layout sections
            foreach (['layout', 'mobileLayout'] as $key) {
                if (!$objPage->$key) {
                    continue;
                }

                $objLayout = LayoutModel::findByPk($objPage->$key);

                if (null === $objLayout) {
                    continue;
                }

                $arrModules = StringUtil::deserialize($objLayout->modules);

                if (empty($arrModules) || !\is_array($arrModules)) {
                    continue;
                }

                // Find all sections with an article module (see #6094)
                foreach ($arrModules as $arrModule) {
                    if (0 === $arrModule['mod'] && $arrModule['enable']) {
                        $arrSections[] = $arrModule['col'];
                    }
                }
            }
        } else {
            // Show all sections (e.g. "override all" mode)
            $arrSections = ['header', 'left', 'right', 'main', 'footer'];
            $objLayout = $this->Database->query("SELECT sections FROM tl_layout WHERE sections!=''");

            while ($objLayout->next()) {
                $arrCustom = StringUtil::deserialize($objLayout->sections);

                // Add the custom layout sections
                if (!empty($arrCustom) && \is_array($arrCustom)) {
                    foreach ($arrCustom as $v) {
                        if (!empty($v['id'])) {
                            $arrSections[] = $v['id'];
                        }
                    }
                }
            }
        }

        return Backend::convertLayoutSectionIdsToAssociativeArray($arrSections);
    }

    /**
     * Return all module templates as array.
     *
     * @return array
     */
    public function getArticleTemplates()
    {
        return $this->getTemplateGroup('mod_article');
    }

    /**
     * Return the edit article button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function editArticle($row, $href, $label, $title, $icon, $attributes)
    {
        $limitAccess = true;

        if ($this->User->isAdmin) {
            $limitAccess = false;
        }

        return ($this->User->hasAccess(
            'edit',
            'themecontent_section_articlep'
        ) || false === $limitAccess) ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'
        " title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label)
            .'</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Return the edit header button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        if (!$this->User->canEditFieldsOf('tl_theme_section')) {
            return Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
        }

        $limitAccess = true;

        if ($this->User->isAdmin) {
            $limitAccess = false;
        }

        return ($this->User->hasAccess(
            'editHeader',
            'themecontent_section_articlep'
        ) || false === $limitAccess)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Return the copy article button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     * @param string $table
     *
     * @return string
     */
    public function copyArticle($row, $href, $label, $title, $icon, $attributes, $table)
    {
        if ($GLOBALS['TL_DCA'][$table]['config']['closed']) {
            return '';
        }

        $limitAccess = true;

        if ($this->User->isAdmin) {
            $limitAccess = false;
        }

        return ($this->User->hasAccess(
            'copy',
            'themecontent_section_articlep'
        ) || false === $limitAccess)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Return the cut article button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function cutArticle($row, $href, $label, $title, $icon, $attributes)
    {
        $limitAccess = true;

        if ($this->User->isAdmin) {
            $limitAccess = false;
        }

        return ($this->User->hasAccess(
            'cut',
            'themecontent_section_articlep'
        ) || false === $limitAccess)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Return the paste article button.
     *
     * @param array  $row
     * @param string $table
     * @param bool   $cr
     * @param array  $arrClipboard
     *
     * @return string
     */
    public function pasteArticle(DataContainer $dc, $row, $table, $cr, $arrClipboard = null)
    {
        $imagePasteAfter = Image::getHtml(
            'pasteafter.svg',
            sprintf($GLOBALS['TL_LANG'][$dc->table]['pasteafter'][1], $row['id'])
        );
        $imagePasteInto = Image::getHtml(
            'pasteinto.svg',
            sprintf($GLOBALS['TL_LANG'][$dc->table]['pasteinto'][1], $row['id'])
        );

        if ($table === $GLOBALS['TL_DCA'][$dc->table]['config']['ptable']) {
            $request = 'act='.$arrClipboard['mode'].'&amp;mode=2&amp;pid='.$row['id'];
            $request .= (!\is_array($arrClipboard['id']) ? '&amp;id='.$arrClipboard['id'] : '');

            return ('root' === $row['type'] || $cr)
                ? Image::getHtml('pasteinto_.svg').' '
                : '<a href="'.$this->addToUrl($request).'" title="'.StringUtil::specialchars(
                    sprintf($GLOBALS['TL_LANG'][$dc->table]['pasteinto'][1], $row['id'])
                ).'" onclick="Backend.getScrollOffset()">'.$imagePasteInto.'</a> ';
        }

        return (('cut' === $arrClipboard['mode'] && $arrClipboard['id'] === $row['id'])
            || ('cutAll' === $arrClipboard['mode'] && \in_array($row['id'], $arrClipboard['id'], true))
            || $cr)
            ? Image::getHtml('pasteafter_.svg').' '
            : '<a href="'.$this->addToUrl(
                'act='.$arrClipboard['mode'].'&amp;mode=1&amp;pid='.$row['id'].(!\is_array($arrClipboard['id'])
                    ? '&amp;id='.$arrClipboard['id']
                    : '')
            ).'" title="'.StringUtil::specialchars(
                sprintf($GLOBALS['TL_LANG'][$dc->table]['pasteafter'][1], $row['id'])
            ).'" onclick="Backend.getScrollOffset()">'.$imagePasteAfter.'</a> ';
    }

    /**
     * Return the delete article button.
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function deleteArticle($row, $href, $label, $title, $icon, $attributes)
    {
        $limitAccess = true;

        if ($this->User->isAdmin) {
            $limitAccess = false;
        }

        return ($this->User->hasAccess(
            'delete',
            'themecontent_section_articlep'
        ) || false === $limitAccess)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" 
                title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * Automatically generate the folder URL aliases.
     *
     * @param array $arrButtons
     *
     * @return array
     */
    public function addAliasButton($arrButtons)
    {
        // Generate the aliases
        if ('tl_select' === Input::post('FORM_SUBMIT') && isset($_POST['alias'])) {
            /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
            $objSession = System::getContainer()->get('session');

            $session = $objSession->all();
            $ids = $session['CURRENT']['IDS'];

            foreach ($ids as $id) {
                $objArticle = ArticleModel::findByPk($id);

                if (null === $objArticle) {
                    continue;
                }

                // Set the new alias
                $strAlias = StringUtil::generateAlias($objArticle->title);

                // The alias has not changed
                if ($strAlias === $objArticle->alias) {
                    continue;
                }

                // Initialize the version manager
                $objVersions = new Versions('tl_theme_section_article', $id);
                $objVersions->initialize();

                // Store the new alias
                $this->Database->prepare('UPDATE tl_theme_section_article SET alias=? WHERE id=?')
                    ->execute($strAlias, $id)
                ;

                // Create a new version
                $objVersions->create();
            }

            $this->redirect($this->getReferer());
        }

        // Add the button
        $arrButtons['alias'] = '<button type="submit" name="alias" id="alias" class="tl_submit" accesskey="a">
                                '.$GLOBALS['TL_LANG']['MSC']['aliasSelected'].'</button> ';

        return $arrButtons;
    }
}
