<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\EventListener\Dca;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\DataContainer;
use Contao\Image;
use Contao\StringUtil;
use Contao\System;
use Doctrine\DBAL\Connection;

/**
 * Handles DCA callbacks/events.
 */
class ContentListener
{
    /**
     * @var ContaoFramework
     */
    protected $framework;

    /**
     * @var Connection
     */
    protected $db;

    /**
     * Constructor.
     */
    public function __construct(ContaoFramework $framework, Connection $db)
    {
        $this->framework = $framework;
        $this->db = $db;
    }

    public function onOptionsThemeArticle(DataContainer $dc)
    {
        $this->framework->initialize();

        $aliases = [];
        $query = null;

        if ('tl_content' === $dc->table) {
            $query = $this->db->executeQuery(
                'SELECT a.id, a.pid, a.title, a.inColumn, p.title AS parent 
                        FROM tl_theme_section_article a 
                            LEFT JOIN tl_theme_section p ON p.id=a.pid 
                        WHERE a.id!=(SELECT pid FROM tl_content WHERE id=?)
                        ORDER BY parent, a.sorting',
                [$dc->id]
            );
        } else {
            $query = $this->db->executeQuery(
                'SELECT a.id, a.pid, a.title, a.inColumn, p.title AS parent 
                        FROM tl_theme_section_article a 
                            LEFT JOIN tl_theme_section p ON p.id=a.pid 
                        ORDER BY parent, a.sorting'
            );
        }

        if ($query->rowCount()) {
            System::loadLanguageFile('tl_article');

            foreach ($query->fetchAll() as $result) {
                $key = $result['parent'];
                $aliases[$key][$result['id']] = $result['title'].' (ID '.$result['id'].')';
            }
        }

        return $aliases;
    }

    public function onEditThemeArticle(DataContainer $dc)
    {
        $this->framework->initialize();

        return ($dc->value < 1) ? '' : ' <a href="contao/main.php?do=theme_content&amp;table=tl_content&amp;id='
            .$dc->value.'&amp;popup=1&amp;nb=1&amp;rt='.REQUEST_TOKEN.'" 
        title="'.sprintf(StringUtil::specialchars($GLOBALS['TL_LANG']['tl_content']['editalias'][1]), $dc->value).'" 
        onclick="Backend.openModalIframe({\'title\':\''.StringUtil::specialchars(
            str_replace(
                "'",
                "\\'",
                sprintf($GLOBALS['TL_LANG']['tl_content']['editalias'][1], $dc->value)
            )
        ).'\',\'url\':this.href});return false;">'
            .Image::getHtml('alias.svg', $GLOBALS['TL_LANG']['tl_content']['editalias'][0]).'</a>';
    }
}
