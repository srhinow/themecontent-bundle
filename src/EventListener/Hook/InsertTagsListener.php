<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension themecontent-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL 3.0 or later
 */

namespace Srhinow\ThemecontentBundle\EventListener\Hook;

use Contao\ContentModel;
use Contao\Controller;
use Contao\CoreBundle\Framework\ContaoFramework;
use Srhinow\ThemecontentBundle\Model\ThemeSectionArticleModel;
use Srhinow\ThemecontentBundle\Module\ModuleThemeArticle;

/**
 * Handles insert tags for news.
 */
class InsertTagsListener
{
    /**
     * @var ContaoFramework
     */
    private $framework;

    /**
     * @var array
     */
    private $supportedTags = [
        'insert_theme_article',
        'insert_theme_content',
    ];

    /**
     * Constructor.
     */
    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
    }

    /**
     * Replaces news insert tags.
     *
     * @param string $tag
     *
     * @return string|false
     */
    public function onReplaceInsertTags($tag)
    {
        $elements = explode('::', $tag);
        $key = strtolower($elements[0]);

        if (\in_array($key, $this->supportedTags, true)) {
            switch ($key) {
                case 'insert_theme_article':
                    return $this->replaceThemeArticleInsertTags($elements[1]);
                    break;
                case 'insert_theme_content':
                    return $this->replaceThemeContentInsertTags($elements[1]);
                    break;
            }
        }

        return false;
    }

    /**
     * Replaces a THEME-ARTICLE-related insert tag.
     *
     * @param string $idOrAlias
     *
     * @return string
     */
    private function replaceThemeArticleInsertTags($idOrAlias)
    {
        $this->framework->initialize();

        /** @var ThemeSectionArticleModel $adapter */
        $adapter = $this->framework->getAdapter(ThemeSectionArticleModel::class);

        if (null === ($objRow = $adapter->findByIdOrAlias($idOrAlias))) {
            return '';
        }

        $objThemeArticle = new ModuleThemeArticle($objRow);

        return $objThemeArticle->generate(true);
    }

    /**
     * Replaces a THEME-CONTENT-related insert tag.
     *
     * @param string $idOrAlias
     *
     * @return string
     */
    private function replaceThemeContentInsertTags($idOrAlias)
    {
        $this->framework->initialize();

        /** @var ContentModel $adapter */
        $adapter = $this->framework->getAdapter(ContentModel::class);

        if (null === ($objRow = $adapter->findByIdOrAlias($idOrAlias))) {
            return '';
        }

        return Controller::getContentElement($objRow->id);
    }
}
